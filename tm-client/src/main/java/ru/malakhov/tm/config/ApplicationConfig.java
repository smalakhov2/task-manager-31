package ru.malakhov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.malakhov.tm.endpoint.*;

@Configuration
@ComponentScan("ru.malakhov.tm")
public class ApplicationConfig {

    @Bean
    @NotNull
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint(
            @NotNull final SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public AdminDataEndpointService adminDataEndpointService() {
        return new AdminDataEndpointService();
    }

    @Bean
    @NotNull
    public AdminDataEndpoint adminDataEndpoint(
            @NotNull final AdminDataEndpointService adminDataEndpointService
    ) {
        return adminDataEndpointService.getAdminDataEndpointPort();
    }

    @Bean
    @NotNull
    public AdminUserEndpointService adminUserEndpointService() {
        return new AdminUserEndpointService();
    }

    @Bean
    @NotNull
    public AdminUserEndpoint adminUserEndpoint(
            @NotNull final AdminUserEndpointService adminUserEndpointService
    ) {
        return adminUserEndpointService.getAdminUserEndpointPort();
    }

    @Bean
    @NotNull
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint(
            @NotNull final UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint(
            @NotNull final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint(
            @NotNull final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

}