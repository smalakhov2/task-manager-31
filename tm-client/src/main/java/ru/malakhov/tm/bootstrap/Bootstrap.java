package ru.malakhov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.exception.unknown.UnknownArgumentException;
import ru.malakhov.tm.exception.unknown.UnknownCommandException;
import ru.malakhov.tm.service.CommandService;
import ru.malakhov.tm.service.PropertyService;
import ru.malakhov.tm.util.TerminalUtil;

import java.util.Collection;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private CommandService commandService;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    private void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        for (@Nullable final String arg : args) {
            try {
                parseArg(arg);
            } catch (@NotNull final Exception e) {
                logError(e);
            }
        }
    }

    private void parseArg(@Nullable final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands().values();
        for (@NotNull final AbstractCommand command : commands) {
            if (arg.equals(command.arg())) {
                command.execute();
                return;
            }
        }
        throw new UnknownArgumentException(arg);
    }

    private void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        @NotNull final AbstractCommand command = commandService.getCommands().get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        if (command.secure() && !propertyService.isAuth()) {
            System.out.println("[ACCESS DENIED]");
            return;
        }
        command.execute();
    }

    public void run(@NotNull final String[] args) {
        commandService.registryCommand();
        parseArgs(args);
        displayWelcome();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (@NotNull final Exception e) {
                logError(e);
            }
        }
    }

    private void logError(@NotNull Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        System.out.println();
    }

}