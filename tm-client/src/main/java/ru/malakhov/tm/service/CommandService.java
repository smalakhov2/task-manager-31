package ru.malakhov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.api.service.ICommandService;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.Map;
import java.util.Set;


@Getter
@Setter
@Service
@NoArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Override
    public void registryCommand() {
        commandRepository.registryCommand();
    }

    @NotNull
    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @NotNull
    @Override
    public Set<String> getCommandsName() {
        return commandRepository.getCommandsName();
    }

    @NotNull
    @Override
    public Set<String> getCommandsArg() {
        return commandRepository.getCommandsArg();
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command.name(), command);
    }

}