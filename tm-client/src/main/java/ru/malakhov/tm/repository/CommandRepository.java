package ru.malakhov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@Repository
@NoArgsConstructor
public final class CommandRepository implements ICommandRepository {

    @NotNull
    @Autowired
    private AbstractCommand[] commandList;

    @NotNull
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void registryCommand() {
        for (@NotNull final AbstractCommand command : commandList) {
            commands.put(command.name(), command);
        }
    }

    @Override
    public void add(@NotNull final String name, @NotNull final AbstractCommand command) {
        commands.put(name, command);
    }

    @NotNull
    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @NotNull
    @Override
    public Set<String> getCommandsName() {
        return commands.keySet();
    }

    @NotNull
    @Override
    public Set<String> getCommandsArg() {
        @NotNull final Set<String> args = new LinkedHashSet<>();
        for (@Nullable final AbstractCommand command : this.commands.values()) {
            if (command == null) continue;
            @Nullable final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            args.add(arg);
        }
        return args;
    }

}