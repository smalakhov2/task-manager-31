package ru.malakhov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.api.repository.IPropertyRepository;
import ru.malakhov.tm.endpoint.SessionDto;

@Repository
@NoArgsConstructor
public final class PropertyRepository implements IPropertyRepository {

    @Nullable
    private SessionDto session;

    @Nullable
    @Override
    public SessionDto getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable final SessionDto session) {
        this.session = session;
    }

}