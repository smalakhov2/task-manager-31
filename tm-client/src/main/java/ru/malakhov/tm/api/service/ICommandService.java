package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.Map;
import java.util.Set;

public interface ICommandService {

    void registryCommand();

    void add(@Nullable AbstractCommand command);

    @NotNull
    Map<String, AbstractCommand> getCommands();

    @NotNull
    Set<String> getCommandsName();

    @NotNull
    Set<String> getCommandsArg();

}