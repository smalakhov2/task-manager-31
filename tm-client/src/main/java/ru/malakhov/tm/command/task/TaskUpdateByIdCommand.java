package ru.malakhov.tm.command.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.TaskEndpoint;
import ru.malakhov.tm.service.PropertyService;
import ru.malakhov.tm.util.TerminalUtil;

@Getter
@Setter
@Component
public final class TaskUpdateByIdCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NEW NAME: ");
        @NotNull final String newName = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        @NotNull final String newDescription = TerminalUtil.nextLine();
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final Result result = taskEndpoint.updateTaskById(
                session,
                id,
                newName,
                newDescription
        );
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}