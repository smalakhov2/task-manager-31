package ru.malakhov.tm.command.admin.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.AdminUserEndpoint;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.service.PropertyService;
import ru.malakhov.tm.util.TerminalUtil;

@Getter
@Setter
@Component
public final class UserLockCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String name() {
        return "user-lock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user account by login.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[LOCK-USER]");
        System.out.print("ENTER USER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final Result result = adminUserEndpoint.lockUserByLogin(session, login);
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}