package ru.malakhov.tm.command.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.ProjectEndpoint;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.service.PropertyService;
import ru.malakhov.tm.util.TerminalUtil;

@Getter
@Setter
@Component
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("ENTER PROJECT NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER PROJECT DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final Result result = projectEndpoint.createProject(session, name, description);
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}