package ru.malakhov.tm.command.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.service.CommandService;

import java.util.Set;

@Getter
@Setter
@Component
public final class ListNameCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private CommandService commandService;

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Set<String> commandsName = commandService.getCommandsName();
        for (final String name : commandsName) System.out.println(name);
    }

    @Override
    public boolean secure() {
        return false;
    }

}