package ru.malakhov.tm.command.admin.data;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.AdminDataEndpoint;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.service.PropertyService;

import java.io.IOException;

@Getter
@Setter
@Component
public final class DataBase64ClearCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove Base64 data.";
    }

    @Override
    public void execute() throws IOException, AbstractException_Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final Result result = adminDataEndpoint.clearDataBase64(session);
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}