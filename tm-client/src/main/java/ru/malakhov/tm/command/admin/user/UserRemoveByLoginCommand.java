package ru.malakhov.tm.command.admin.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.*;
import ru.malakhov.tm.service.PropertyService;
import ru.malakhov.tm.util.TerminalUtil;

@Getter
@Setter
@Component
public class UserRemoveByLoginCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "user-remove";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[USER-REMOVE]");
        System.out.print("ENTER USER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final SessionDto session = propertyService.getSession();
        @Nullable final UserDto currentUser = sessionEndpoint.getUser(session);
        if (currentUser == null) {
            System.out.println("[Error]");
            return;
        }
        if (login.equals(currentUser.getLogin())) {
            String answer = "";
            do {
                System.out.print("THIS IS YOUR ACCOUNT. DELETE IT(Y/N): ");
                answer = TerminalUtil.nextLine().toLowerCase();
            } while (!answer.equals("y") && !answer.equals("n"));
            if (answer.equals("n")) {
                System.out.println("[CANCEL]");
                return;
            }
            propertyService.setSession(null);
        }
        @NotNull final Result result = adminUserEndpoint.removeUserByLogin(
                session,
                login
        );
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}