package ru.malakhov.tm.command.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.service.CommandService;

import java.util.Collection;

@Getter
@Setter
@Component
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private CommandService commandService;

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands().values();
        for (@NotNull final AbstractCommand command : commands) System.out.println(command);
    }

    @Override
    public boolean secure() {
        return false;
    }

}