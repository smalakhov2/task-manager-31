package ru.malakhov.tm.command.authentication;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.SessionEndpoint;
import ru.malakhov.tm.service.PropertyService;

@Getter
@Setter
@Component
public final class LogoutCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final Result result = sessionEndpoint.closeSession(session);
        if (result.isSuccess()) {
            System.out.println("[OK]");
            propertyService.setSession(null);
        } else System.out.println("[FAIL]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}