package ru.malakhov.tm.command.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.service.CommandService;

import java.util.Set;

@Getter
@Setter
@Component
public final class ListArgCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private CommandService commandService;

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Set<String> commandsArg = commandService.getCommandsArg();
        for (@NotNull final String arg : commandsArg) System.out.println(arg);
    }

    @Override
    public boolean secure() {
        return false;
    }

}