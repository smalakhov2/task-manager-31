package ru.malakhov.tm.command.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.TaskDto;
import ru.malakhov.tm.endpoint.TaskEndpoint;
import ru.malakhov.tm.service.PropertyService;

import java.util.List;

@Getter
@Setter
@Component
public final class TaskListShowCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[LIST TASKS]");
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final List<TaskDto> tasks = taskEndpoint.getTaskList(session);
        int index = 1;
        for (final TaskDto task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}