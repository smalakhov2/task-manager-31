package ru.malakhov.tm.command.authentication;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.UserEndpoint;
import ru.malakhov.tm.util.TerminalUtil;

@Getter
@Setter
@Component
public final class RegistryCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Registry.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final Result result = userEndpoint.registryUser(login, password, email);
        if (result.isSuccess()) {
            System.out.println("[OK]");
        } else System.out.println("[FAIL]");
    }

    @Override
    public boolean secure() {
        return false;
    }

}