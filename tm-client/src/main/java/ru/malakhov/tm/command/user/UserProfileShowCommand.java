package ru.malakhov.tm.command.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.SessionEndpoint;
import ru.malakhov.tm.endpoint.UserDto;
import ru.malakhov.tm.service.PropertyService;

@Getter
@Setter
@Component
public final class UserProfileShowCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "show-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show profile.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[YOUR-PROFILE]");
        @Nullable final SessionDto session = propertyService.getSession();
        @Nullable final UserDto user = sessionEndpoint.getUser(session);
        if (user == null) {
            System.out.println("[Error]");
            return;
        }
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("ROLE: " + user.getRole().value());
    }

    @Override
    public boolean secure() {
        return true;
    }

}