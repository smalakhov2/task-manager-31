package ru.malakhov.tm.command.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.ProjectDto;
import ru.malakhov.tm.endpoint.ProjectEndpoint;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.service.PropertyService;
import ru.malakhov.tm.util.TerminalUtil;

@Getter
@Setter
@Component
public final class ProjectShowByIdCommand extends AbstractProjectShowCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final SessionDto session = propertyService.getSession();
        @Nullable final ProjectDto project = projectEndpoint.getProjectById(session, id);
        if (project == null) System.out.println("[FAIL]");
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}