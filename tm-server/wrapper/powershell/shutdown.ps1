write-host  "SHUTDOWN SERVER..."

if ( -not ( Test-Path "server.pid" ) ) {
    write-host  "server.pid not found!"
    Wait-Event -Timeout 5
    exit 1
}

$javapid = Get-Content server.pid
write-host  "KILL PROCESS WITH PID $pid"
Stop-Process -Id $javapid -Force
Remove-Item server.pid
Wait-Event -Timeout 5
write-host  "OK"