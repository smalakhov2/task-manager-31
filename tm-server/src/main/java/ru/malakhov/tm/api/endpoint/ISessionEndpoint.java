package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ISessionEndpoint extends IEndpoint {

    @NotNull
    @WebMethod
    Result clearAllSession(
            @WebParam(name = "session") @Nullable final SessionDto session
    );

    List<SessionDto> getAllSessionList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException;

    @Nullable
    @WebMethod
    SessionDto openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result closeSession(@WebParam(name = "session") @Nullable SessionDto session);

    @NotNull
    @WebMethod
    Result closeSessionAll(@WebParam(name = "session") @Nullable SessionDto session);

    @NotNull
    @WebMethod
    List<SessionDto> getListSession(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @Nullable
    @WebMethod
    UserDto getUser(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

}