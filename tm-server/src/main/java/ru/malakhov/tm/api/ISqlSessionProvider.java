package ru.malakhov.tm.api;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface ISqlSessionProvider {

    @NotNull
    EntityManager getEntityManager();

}