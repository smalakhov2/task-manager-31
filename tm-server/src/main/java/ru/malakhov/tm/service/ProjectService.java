package ru.malakhov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IndexIncorrectException;
import ru.malakhov.tm.repository.ProjectRepository;

import java.util.List;

@Service
@NoArgsConstructor
public final class ProjectService extends AbstractService<ProjectDto, IProjectRepository> implements IProjectService {

    @Override
    public @NotNull IProjectRepository getRepository() {
        return context.getBean(ProjectRepository.class);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final ProjectDto project = new ProjectDto(name, "", userId);
        persist(project);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final ProjectDto project = new ProjectDto(name, description, userId);
        persist(project);
    }

    @NotNull
    @Override
    public List<ProjectDto> findAllDto() {
        @NotNull final IProjectRepository repository = getRepository();
        return repository.findAllDto();
    }

    @NotNull
    @Override
    public List<Project> findAllEntity() {
        @NotNull final IProjectRepository repository = getRepository();
        return repository.findAllEntity();
    }

    @NotNull
    public List<ProjectDto> findAllDtoByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findAllDtoByUserId(userId);
    }

    @NotNull
    public List<Project> findAllEntityByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findAllEntityByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findOneDtoById(id);
    }

    @Nullable
    @Override
    public Project findOneEntityById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findOneEntityById(id);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findOneDtoById(userId, id);
    }

    @Nullable
    @Override
    public Project findOneEntityById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findOneEntityById(userId, id);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findOneDtoByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project findOneEntityByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findOneEntityByIndex(userId, index);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findOneDtoByName(userId, name);
    }

    @Nullable
    @Override
    public Project findOneEntityByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final IProjectRepository repository = getRepository();
        return repository.findOneEntityByName(userId, name);
    }

    @Override
    public void removeAll() {
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll();
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAllByUserId(userId);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(id);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(userId, id);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneByIndex(userId, index);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneByName(userId, name);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final ProjectDto project = findOneDtoById(userId, id);
        if (project == null) return;
        project.setName(name);
        project.setDescription(description);
        merge(project);
    }

    @Override
    public void updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final ProjectDto project = findOneDtoByIndex(userId, index);
        if (project == null) return;
        project.setName(name);
        project.setDescription(description);
        merge(project);
    }

}