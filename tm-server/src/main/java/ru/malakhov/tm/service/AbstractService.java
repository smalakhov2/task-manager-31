package ru.malakhov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.api.service.IService;
import ru.malakhov.tm.dto.AbstractEntityDto;

import java.util.Collection;
import java.util.Objects;

@Service
@NoArgsConstructor
public abstract class AbstractService<E extends AbstractEntityDto, R extends IRepository<E>> implements IService<E, R> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @Override
    public void persist(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.persist(entity);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void persist(@Nullable final Collection<E> entities) {
        if (entities == null) return;
        entities.removeIf(Objects::isNull);
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.persist(entities);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SafeVarargs
    public final void persist(@Nullable final E... entities) {
        if (entities == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.persist(entities);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void merge(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.merge(entity);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void merge(@Nullable final Collection<E> entities) {
        if (entities == null) return;
        entities.removeIf(Objects::isNull);
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.merge(entities);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SafeVarargs
    public final void merge(@Nullable final E... entities) {
        if (entities == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.merge(entities);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeOne(@Nullable E entity) {
        if (entity == null) return;
        @NotNull final R repository = getRepository();

        try {
            repository.begin();
            repository.removeOne(entity);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public abstract R getRepository();

}
