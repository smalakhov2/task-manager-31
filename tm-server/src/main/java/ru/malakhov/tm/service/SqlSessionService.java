package ru.malakhov.tm.service;

import lombok.NoArgsConstructor;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.ISqlSessionProvider;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.api.service.ISqlSessionService;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Service
@NoArgsConstructor
public final class SqlSessionService implements ISqlSessionService, ISqlSessionProvider {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    private EntityManagerFactory entityManagerFactory;

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getJdbcDriver());
        settings.put(Environment.URL, propertyService.getJdbcUrl());
        settings.put(Environment.USER, propertyService.getJdbcUsername());
        settings.put(Environment.PASS, propertyService.getJdbcPassword());
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(TaskDto.class);
        sources.addAnnotatedClass(ProjectDto.class);
        sources.addAnnotatedClass(UserDto.class);
        sources.addAnnotatedClass(SessionDto.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @PostConstruct
    public void initFactory() {
        entityManagerFactory = factory();
    }

    public void closeFactory() {
        entityManagerFactory.close();
    }

    @NotNull
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}