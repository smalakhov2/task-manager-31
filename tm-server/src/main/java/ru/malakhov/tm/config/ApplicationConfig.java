package ru.malakhov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.malakhov.tm.api.service.ISqlSessionService;

import javax.persistence.EntityManager;

@Configuration
@ComponentScan("ru.malakhov.tm")
public class ApplicationConfig {

    @Bean
    @NotNull
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public EntityManager entityManager(@NotNull final ISqlSessionService sqlSessionService) {
        return sqlSessionService.getEntityManager();
    }

}