package ru.malakhov.tm.dto;

public class ServerDto {

    public String port = "";

    public String host = "";

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

}