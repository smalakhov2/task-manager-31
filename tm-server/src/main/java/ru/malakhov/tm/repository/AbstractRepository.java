package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.dto.AbstractEntityDto;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractRepository<E extends AbstractEntityDto> implements IRepository<E> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Override
    public void begin() {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commit() {
        entityManager.getTransaction().commit();
    }

    @Override
    public void rollback() {
        entityManager.getTransaction().rollback();
    }

    @Override
    public void close() {
        entityManager.close();
    }

    @Override
    public void clear() {
        entityManager.clear();
    }

    @Override
    public void persist(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    public void persist(@NotNull final Collection<E> entities) {
        for (@NotNull final E entity : entities) {
            entityManager.persist(entity);
        }
    }

    @Override
    @SafeVarargs
    public final void persist(@Nullable final E... entities) {
        for (E entity : entities) if (entity != null) entityManager.persist(entity);
    }

    @Override
    public void merge(@NotNull final E entity) {
        entityManager.merge(entity);
    }

    @Override
    public void merge(@NotNull final Collection<E> entities) {
        for (@NotNull final E entity : entities) {
            entityManager.merge(entity);
        }
    }

    @Override
    @SafeVarargs
    public final void merge(@Nullable final E... entities) {
        for (E entity : entities) if (entity != null) entityManager.merge(entity);
    }

    @Override
    public void removeOne(@NotNull E entity) {
        entityManager.remove(entity);
    }

}
