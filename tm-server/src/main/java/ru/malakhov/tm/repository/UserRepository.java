package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.User;

import java.util.List;

@Repository
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public final class UserRepository extends AbstractRepository<UserDto> implements IUserRepository {

    @NotNull
    @Override
    public List<UserDto> findAllDto() {
        return entityManager.createQuery("SELECT e FROM UserDto e", UserDto.class).getResultList();
    }

    @NotNull
    @Override
    public List<User> findAllEntity() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @Nullable
    @Override
    public UserDto findOneDtoById(@NotNull final String id) {
        @NotNull final List<UserDto> users =
                entityManager.createQuery("SELECT e FROM UserDto e WHERE e.id=:id", UserDto.class)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @Nullable
    @Override
    public User findOneEntityById(@NotNull final String id) {
        @NotNull final List<User> users =
                entityManager.createQuery("SELECT e FROM User e WHERE e.id=:id", User.class)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @Nullable
    @Override
    public UserDto findOneDtoByLogin(@NotNull final String login) {
        @NotNull final List<UserDto> users =
                entityManager.createQuery("SELECT e FROM UserDto e WHERE e.login=:login", UserDto.class)
                        .setParameter("login", login)
                        .setMaxResults(1)
                        .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @Nullable
    @Override
    public User findOneEntityByLogin(@NotNull final String login) {
        @NotNull final List<User> users =
                entityManager.createQuery("SELECT e FROM User e WHERE e.login=:login", User.class)
                        .setParameter("login", login)
                        .setMaxResults(1)
                        .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @Override
    public void removeAll() {
        @NotNull final List<User> users = findAllEntity();
        for (@NotNull final User user : users) entityManager.remove(user);
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final User user = findOneEntityById(id);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public void removeOneByLogin(@NotNull final String login) {
        @Nullable final User user = findOneEntityByLogin(login);
        if (user == null) return;
        entityManager.remove(user);
    }

}