package ru.malakhov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.malakhov.tm.api.endpoint.IAdminDataEndpoint;
import ru.malakhov.tm.api.service.IAdminService;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.dto.ServerDto;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
@NoArgsConstructor
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IAdminService adminService;

    @NotNull
    @Override
    @WebMethod
    public Result saveDataBinary(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveDataBinary();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataBinary(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.loadDataBinary();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataBinary(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.clearDataBinary();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataBase64(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveDataBase64();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataBase64(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.loadDataBase64();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataBase64(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.clearDataBase64();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataJson(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveDataJson();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataJson(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.loadDataJson();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataJson(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.clearDataJson();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataXml(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveDataXml();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataXml(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.loadDataXml();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataXml(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.clearDataXml();
            return new Success();
        } catch (@NotNull final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ServerDto getServerInfo(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        return adminService.getServerInfo();
    }

}